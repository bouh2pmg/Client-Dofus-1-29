# Client Dofus 1.29

Ce projet est uniquement destiné à héberger les fichiers nécessaires pour exécuter Dofus 1.29 sur Windows, Mac et Linux.
Le contenu n'a pas été altéré d'une quelconque façon. Il s'agit de la version officielle du jeu Dofus.

##### Dofus est la propriété de Ankama Games et nous ne sommes en aucun cas affiliés à Ankama. Ce projet est uniquement voué à être exploité à des fins éducatives ; nous ne gagnerons jamais d'argent avec ce projet.

### Installation

Afin d'installer ce projet, 2 choix s'offrent à vous :
- Le téléchargement depuis l'interface de Github ;
- L'utilisation de git a travers la ligne de commande :

```bash
cd /path/of/your/folder/games
```

__

```bash
git clone https://github.com/pimpmygame/Client-Dofus-1.29.git
```
ou
```bash
git clone git@github.com:pimpmygame/Client-Dofus-1.29.git
```

### Liens
###### Windows
* http://download.dofus.com/dofus1/win/
###### MacOs
* http://dl.ak.ankama.com/games/dofus/client/mac/Dofus_1_MacOsX_Installer.dmg
###### Linux
* http://download.dofus.com/dofus1/linux/


Une fois le fichier "Dofus_1_Installer" téléchargé, il vous suffit de double-cliquer dessus afin de le lancer et de suivre les étapes d'installation.

Bon jeu à tous sur DOFUS 1.29 !

*Sur certaines machines, la version Mac OS n'est plus supportée. 


### Auteur
Ce projet est mis à disposition par la communauté PimpMyGame.
https://pimpmygame.fr/
